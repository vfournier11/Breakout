package com.ntfournier.breakout.simulation.actors;

public class IndestructibleBrick extends Brick {
    public IndestructibleBrick(float x, float y) {
        super(x, y);
    }

    @Override
    public void hit() {
        // Do nothing
    }
}
