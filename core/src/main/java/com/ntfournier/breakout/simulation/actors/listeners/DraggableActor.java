package com.ntfournier.breakout.simulation.actors.listeners;

import com.badlogic.gdx.scenes.scene2d.*;

public class DraggableActor extends InputListener {
    Actor actor;

    public DraggableActor(Actor actor) {
        this.actor = actor;
    }

    @Override
    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
        return true;
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        actor.moveBy(x - actor.getWidth() / 2, y - actor.getHeight() / 2);
    }
}
