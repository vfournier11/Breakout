package com.ntfournier.breakout.simulation;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.Constants;
import com.ntfournier.breakout.simulation.actors.*;
import com.ntfournier.breakout.simulation.actors.powerups.PowerUp;

import java.util.*;

import static com.ntfournier.breakout.Constants.*;

/**
 * Simulation, manage collisions between actors.
 */
public class Simulation {
    public final Stage stage;
    public ActorsFactory actorsFactory;
    public EventHandler gameEventHandler;
    Paddle paddle;
    ArrayList<Ball> balls;
    ArrayList<Brick> bricks;
    ArrayList<PowerUp> powerUps;

    public Simulation(EventHandler gameEventHandler, int width, int height, Stage stage) {
        this.gameEventHandler = gameEventHandler;
        this.stage = stage;

        this.balls = new ArrayList<>();
        this.bricks = new ArrayList<>();
        this.powerUps = new ArrayList<>();

        this.actorsFactory = new ActorsFactory(this, width, height);
        this.paddle = this.actorsFactory.createPaddle();
        this.stage.addActor(this.paddle);
    }

    public ArrayList<Ball> getBalls() {
        return balls;
    }

    public ArrayList<Brick> getBricks() {
        return bricks;
    }

    public Paddle getPaddle() {
        return paddle;
    }

    public void update(float deltaTime) {
        for (int i = 0; i < this.balls.size(); ++i) {
            Ball ball = this.balls.get(i);
            ball.act(deltaTime);
            this.checkCollision(ball);
        }

        for (int i = 0; i < this.powerUps.size(); ) {
            PowerUp powerUp = this.powerUps.get(i);
            powerUp.act(deltaTime);
            if (powerUp.overlaps(this.paddle)) {
                this.gameEventHandler.handle(Constants.EVENT_POWER_UP_COLLECTED, powerUp);
                powerUp.remove();
                powerUps.remove(i);
            } else {
                ++i;
            }
        }
    }

    public void checkCollision(Ball ball) {
        if (paddle.overlaps(ball)) {
            Vector2 newDirection = paddle.getBounceDirection(ball.getX(Align.center));
            ball.setDirection(newDirection.x, newDirection.y);
            ball.setY(paddle.getY() + PADDLE_HEIGHT);

            this.gameEventHandler.handle(Constants.EVENT_PADDLE_HIT, ball);
        }

        int closestBrickIdx = -1;
        float closestBrickDistance = Float.MAX_VALUE;
        for (int i = 0; i < bricks.size(); ++i) {
            Brick brick = bricks.get(i);
            if (brick.overlaps(ball)) {
                float distance = Vector2.dst(brick.getX() + BRICK_SIZE / 2, brick.getY() + BRICK_SIZE / 2,
                                             ball.getX() + BALL_SIZE / 2, ball.getY() + BALL_SIZE / 2);
                if (distance < closestBrickDistance) {
                    closestBrickDistance = distance;
                    closestBrickIdx = i;
                }
            }
        }

        if (closestBrickIdx != -1) {
            Brick brick = bricks.get(closestBrickIdx);

            int align = brick.getNineAreaCollision(ball);
            switch (align) {
                case Align.left:
                    ball.setDirectionLeft();
                    break;
                case Align.bottom:
                    ball.setDirectionDown();
                    break;
                case Align.bottomLeft:
                    ball.setDirectionLeft();
                    ball.setDirectionDown();
                    ball.setDirectionRandom(BALL_DIRECTION_RANDOM);
                    break;
                case Align.right:
                    ball.setDirectionRight();
                    break;
                case Align.top:
                    ball.setDirectionUp();
                    break;
                case Align.bottomRight:
                    ball.setDirectionRight();
                    ball.setDirectionDown();
                    ball.setDirectionRandom(BALL_DIRECTION_RANDOM);
                    break;
                case Align.topLeft:
                    ball.setDirectionUp();
                    ball.setDirectionLeft();
                    ball.setDirectionRandom(BALL_DIRECTION_RANDOM);
                    break;
                case Align.topRight:
                    ball.setDirectionRandom(BALL_DIRECTION_RANDOM);
                    ball.setDirectionUp();
                    ball.setDirectionRight();
                    break;
            }

            brick.hit();
            if (brick.getHitPoints() <= 0) {
                this.removeBrick(brick);
            }
        }
    }

    public void removeBrick(Brick brick) {
        this.gameEventHandler.handle(Constants.EVENT_BRICK_DESTROYED, brick);
        brick.remove();
        this.bricks.remove(brick);

        if (this.bricks.size() == 0) {
            this.gameEventHandler.handle(Constants.EVENT_NO_BRICK_LEFT);
        }

    }

    public void addBricks(ArrayList<Brick> bricks) {
        bricks.forEach((brick) -> {
            this.gameEventHandler.handle(Constants.EVENT_BRICK_CREATED, brick);
            this.bricks.add(brick);
            this.stage.addActor(brick);
        });
    }

    public void addBall(Ball ball) {
        ball.eventHandler.put(Constants.EVENT_BALL_HIT_BOTTOM, (objects) -> {
            ball.remove();
            this.balls.remove(ball);

            this.gameEventHandler.handle(Constants.EVENT_BALL_HIT_BOTTOM, ball);

            if (this.balls.size() == 0) {
                this.gameEventHandler.handle(Constants.EVENT_NO_BALL_LEFT);
            }
        });

        this.gameEventHandler.handle(Constants.EVENT_BALL_CREATED, ball);
        this.balls.add(ball);
        this.stage.addActor(ball);
    }

    public List<PowerUp> getPowerUps() {
        return this.powerUps;
    }

    public void createPowerUp(float x, float y) {
        PowerUp powerUp = actorsFactory.createRandomPowerUp(x, y);
        this.powerUps.add(powerUp);
        this.stage.addActor(powerUp);

        this.gameEventHandler.handle(Constants.EVENT_POWER_UP_CREATED);
    }

    public void startGame() {
        this.addBall(this.actorsFactory.createBallOnPaddle());
    }

    public void dispose() {
        this.stage.dispose();
    }

    public void reset() {
        while (!this.bricks.isEmpty()) {
            this.removeBrick(this.bricks.get(0));
        }
    }
}
