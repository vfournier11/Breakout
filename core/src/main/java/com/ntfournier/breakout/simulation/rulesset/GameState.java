package com.ntfournier.breakout.simulation.rulesset;

public enum GameState {
    GAME_OVER,
    PLAY
}
