package com.ntfournier.breakout.simulation.actors;


import com.badlogic.gdx.math.Vector2;
import com.ntfournier.breakout.Constants;

import static java.lang.Math.random;

public class Ball extends BoundedGameActor {
    protected float speed;
    protected Vector2 direction;

    public Ball(float x, float y, Vector2 direction) {
        super(x, y, Constants.BALL_SIZE, Constants.BALL_SIZE);

        this.speed = Constants.BALL_SPEED;
        this.direction = direction.nor();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        this.setX(this.getX() + this.direction.x * this.speed * delta);
        this.setY(this.getY() + this.direction.y * this.speed * delta);
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public void setDirection(float x, float y) {
        this.direction.x = x;
        this.direction.y = y;
        this.direction.nor();
    }

    public Vector2 getDirection() {
        return this.direction;
    }


    @Override
    protected void outTopBounds() {
        super.outTopBounds();

        this.setDirectionDown();
    }

    @Override
    protected void outBottomBounds() {
        super.outBottomBounds();
        this.eventHandler.handle(Constants.EVENT_BALL_HIT_BOTTOM, this);
    }

    @Override
    protected void outLeftBounds() {
        super.outLeftBounds();

        this.setDirectionRight();
    }

    @Override
    protected void outRightBounds() {
        super.outRightBounds();

        this.setDirectionLeft();
    }

    public void setDirectionRight() {
        this.direction.x = Math.abs(this.direction.x);
    }

    public void setDirectionLeft() {
        this.direction.x = -Math.abs(this.direction.x);
    }

    public void setDirectionDown() {
        this.direction.y = -Math.abs(this.direction.y);
    }

    public void setDirectionUp() {
        this.direction.y = Math.abs(this.direction.y);
    }

    @Override
    public String getLogString() {
        return String.format("%-15s x: %8.2f  y: %8.2f  w: %8.2f  h: %8.2f  dir:  %3.2f, %3.2f",
                             this.getClass().getSimpleName(),
                             this.getX(), this.getY(),
                             this.getWidth(), this.getHeight(), this.direction.x, this.direction.y);
    }

    public void setDirectionRandom(float delta) {
        this.setDirection(this.getDirection().x + (float) (random() * delta) - delta / 2.f,
                          this.getDirection().y + (float) (random() * delta) - delta / 2.f);

    }
}
