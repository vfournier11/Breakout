package com.ntfournier.breakout.simulation.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.ntfournier.breakout.Constants;
import com.ntfournier.breakout.simulation.actors.*;

import java.util.*;

import static com.ntfournier.breakout.Constants.*;

public class LevelFactory {
    public static Level create(int levelNumber) {
        FileHandle handle = Gdx.files.internal("levels/level_" + levelNumber + ".txt");
        String levelDescription = handle.readString();

        LevelProperties levelProperties = new LevelProperties();
        levelProperties.bricks = levelDescription;

        Level level = new Level();
        level.bricks = instantiateBrick(levelProperties.bricks);
        return level;
    }

    protected static ArrayList<Brick> instantiateBrick(String bricksRepresentation) {
        ArrayList<Brick> bricks = new ArrayList<>();

        String[] rows = bricksRepresentation.split("\n");
        Map<Integer, Color> palette = Constants.PALETTES.get(rows[0]);

        String[] b = rows[1].split(",");
        int initialX = (SIMULATION_WIDTH - (b.length * BRICK_SIZE)) / 2;
        int initialY = SIMULATION_HEIGHT - BRICK_SIZE * rows.length; // Same margin in Y and X.
        int x = initialX;
        int y = initialY;
        for (int i = rows.length - 1; i >= 1; --i) {
            b = rows[i].split(",");
            for (int j = 0; j < b.length; ++j) {
                int brickType = Integer.parseInt(b[j]);
                if (brickType == 0) {
                    x += BRICK_SIZE;
                    continue;
                }

                Brick brick;

                switch (brickType) {
                    case 1: // Indestructible brick
                        brick = new IndestructibleBrick(x, y);
                        brick.setColor(palette.get(brickType));
                        break;
                    case 2: // Brick with resistance
                        brick = new SolidBrick(x,
                                               y,
                                               Constants.SOLID_BRICK_DEFAULT_HIT_POINTS,
                                               palette.get(4),
                                               palette.get(3),
                                               palette.get(2));
                        break;
                    case 3:
                    case 4:
                        System.out.println("Not suppose to have bricks with brickType " + brickType + ".");
                    default:
                        brick = new Brick(x, y);
                        brick.setColor(palette.get(brickType));
                        break;
                }
                bricks.add(brick);

                x += BRICK_SIZE;
            }
            x = initialX;
            y += BRICK_SIZE;
        }
        return bricks;
    }
}
