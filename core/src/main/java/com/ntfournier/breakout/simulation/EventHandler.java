package com.ntfournier.breakout.simulation;

import com.ntfournier.breakout.utils.SimpleEvent;

import java.util.*;

public class EventHandler {
    private Map<String, List<SimpleEvent>> gameEventHandler;

    public EventHandler() {
        this.gameEventHandler = new HashMap<>();
    }

    public void put(String key, SimpleEvent simpleEvent) {
        this.gameEventHandler.computeIfAbsent(key, k -> new ArrayList<>());
        this.gameEventHandler.get(key).add(simpleEvent);
    }

    public void handle(String key, Object... objects) {
        List<SimpleEvent> simpleEvents = this.gameEventHandler.get(key);
        if (simpleEvents == null) {
            System.out.println("Simple Events " + key + " not found in EventHandler.");
        } else {
            for (int i = 0; i < simpleEvents.size(); ++i) {
                simpleEvents.get(i).handle(objects);
            }
        }
    }
}
