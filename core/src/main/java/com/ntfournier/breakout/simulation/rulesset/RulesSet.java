package com.ntfournier.breakout.simulation.rulesset;

import com.ntfournier.breakout.simulation.Simulation;
import com.ntfournier.breakout.simulation.actors.*;
import com.ntfournier.breakout.simulation.actors.powerups.PowerUp;

public abstract class RulesSet {
    protected final Simulation simulation;

    public boolean skipNextFrame = false;

    int points = 0;
    String scoreString = "";

    RulesSet(Simulation simulation) {
        this.simulation = simulation;
    }

    public abstract String getScoreString();

    public abstract void ballHitBottom(Ball ball);

    public abstract void noBallsLeftInPlay();

    public abstract void brickDestroyed(Brick brick);

    public abstract void ballHitPaddle(Ball ball);

    public abstract void noBricksLeftInPlay();

    public abstract void ballCreated(Ball ball);

    public abstract void startGame();

    public abstract boolean isGameOver();

    public abstract void powerUpCollected(PowerUp powerUp);
}
