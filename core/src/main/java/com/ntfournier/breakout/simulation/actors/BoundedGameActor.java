package com.ntfournier.breakout.simulation.actors;

import com.badlogic.gdx.math.Rectangle;

public class BoundedGameActor extends GameActor {
    private Rectangle outerBounds;

    public BoundedGameActor(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    public void setOuterBounds(Rectangle outerBounds) {
        this.outerBounds = outerBounds;
    }

    @Override
    protected void positionChanged() {
        if (this.outerBounds != null) {
            if (this.getX() < this.outerBounds.getX()) {
                this.setX(this.outerBounds.getX());
                outLeftBounds();
            } else if (this.getX() + this.getWidth() > this.outerBounds.getX() + this.outerBounds.getWidth()) {
                this.setX(this.outerBounds.getX() + this.outerBounds.getWidth() - this.getWidth());
                outRightBounds();
            }

            if (this.getY() < this.outerBounds.getY()) {
                this.setY(this.outerBounds.getY());
                outBottomBounds();
            } else if (this.getY() + this.getHeight() > this.outerBounds.getY() + this.outerBounds.getHeight()) {
                this.setY(this.outerBounds.getY() + this.outerBounds.getHeight() - this.getHeight());
                outTopBounds();
            }
        }

        super.positionChanged();
    }

    protected void outTopBounds() {
    }

    protected void outBottomBounds() {
    }

    protected void outLeftBounds() {
    }

    protected void outRightBounds() {
    }
}
