package com.ntfournier.breakout.simulation.level;

public class LevelProperties {
    public String bricks = "0,1,1,3,1,1,1,1,3,0";
    public LevelType levelType = LevelType.CLASSIC;

    enum LevelType {
        CLASSIC,
        DEBUG
    }
}
