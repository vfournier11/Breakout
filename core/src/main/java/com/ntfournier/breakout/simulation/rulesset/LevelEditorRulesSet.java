package com.ntfournier.breakout.simulation.rulesset;

import com.ntfournier.breakout.simulation.Simulation;

public class LevelEditorRulesSet extends SimpleRulesSet {
    boolean isCustomLevelComplete = false;

    public LevelEditorRulesSet(Simulation simulation, int currentLevel) {
        super(simulation, currentLevel);
    }

    @Override
    public void noBallsLeftInPlay() {
        this.simulation.addBall(this.simulation.actorsFactory.createBallOnPaddle());
    }

    @Override
    public void noBricksLeftInPlay() {
        this.isCustomLevelComplete = true;
    }

    @Override
    public boolean isGameOver() {
        return this.isCustomLevelComplete;
    }
}
