package com.ntfournier.breakout.simulation.rulesset;

import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.Constants;
import com.ntfournier.breakout.simulation.Simulation;
import com.ntfournier.breakout.simulation.actors.*;
import com.ntfournier.breakout.simulation.actors.powerups.PowerUp;
import com.ntfournier.breakout.simulation.level.*;

public class SimpleRulesSet extends RulesSet {
    private static final String scoreFormat = "Balls: %d -- Score %d";

    private int ballLeft = 1;
    private int currentLevel;

    public SimpleRulesSet(Simulation simulation, int currentLevel) {
        super(simulation);

        this.currentLevel = currentLevel;
    }

    @Override
    public String getScoreString() {
        return this.scoreString;
    }

    @Override
    public void ballHitBottom(Ball ball) {
        this.constructScore();
    }

    @Override
    public void noBallsLeftInPlay() {
        this.ballLeft -= 1;
        if (this.ballLeft > 0) {
            this.simulation.addBall(this.simulation.actorsFactory.createBallOnPaddle());

            this.constructScore();
        }
    }

    @Override
    public void brickDestroyed(Brick brick) {
        this.points += 100;
        if (Math.random() <= Constants.BRICK_POWER_UP_YIELD) {
            this.simulation.createPowerUp(brick.getX(Align.center), brick.getY(Align.center));
        }
        this.constructScore();
    }

    @Override
    public void ballHitPaddle(Ball ball) {
        this.skipNextFrame = true;
    }

    @Override
    public void noBricksLeftInPlay() {
        Level level = LevelFactory.create(++this.currentLevel);
        this.simulation.addBricks(level.bricks);
    }

    @Override
    public void ballCreated(Ball ball) {

    }

    @Override
    public void startGame() {
        Level level = LevelFactory.create(this.currentLevel);
        this.simulation.addBricks(level.bricks);
        this.simulation.startGame();

        this.constructScore();
    }

    @Override
    public boolean isGameOver() {
        return this.ballLeft <= 0;
    }

    @Override
    public void powerUpCollected(PowerUp powerUp) {
        powerUp.activate(this.simulation,
                         this.simulation.getPaddle(),
                         this.simulation.getBalls());
    }

    public void constructScore() {
        this.scoreString = String.format(scoreFormat, this.ballLeft, this.points);
    }
}
