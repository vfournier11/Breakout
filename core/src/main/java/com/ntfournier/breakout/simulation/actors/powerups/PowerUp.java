package com.ntfournier.breakout.simulation.actors.powerups;

import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.Constants;
import com.ntfournier.breakout.simulation.Simulation;
import com.ntfournier.breakout.simulation.actors.*;

import java.util.ArrayList;

public abstract class PowerUp extends GameActor {
    private final float speed;

    public PowerUp(float x, float y) {
        super(x, y, Constants.POWER_UP_WIDTH, Constants.POWER_UP_HEIGHT);
        this.speed = Constants.POWER_UP_SPEED;

        this.setX(x, Align.center);
        this.setY(y, Align.center);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        this.setY(this.getY() - delta * this.speed);
    }

    public abstract void activate(Simulation simulation, Paddle paddle, ArrayList<Ball> balls);
}
