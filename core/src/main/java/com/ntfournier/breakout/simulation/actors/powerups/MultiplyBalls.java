package com.ntfournier.breakout.simulation.actors.powerups;

import com.badlogic.gdx.math.Vector2;
import com.ntfournier.breakout.simulation.Simulation;
import com.ntfournier.breakout.simulation.actors.*;

import java.util.ArrayList;

public class MultiplyBalls extends PowerUp {
    public MultiplyBalls(float x, float y) {
        super(x, y);
    }

    @Override
    public void activate(Simulation simulation, Paddle paddle, ArrayList<Ball> balls) {
        Ball initialBall = balls.get((int) (Math.random() * balls.size()));
        simulation.addBall(
                simulation.actorsFactory.createBall(initialBall.getX(), initialBall.getY(),
                                                    new Vector2(-initialBall.getDirection().x,
                                                                initialBall.getDirection().y))
        );

        simulation.addBall(
                simulation.actorsFactory.createBall(initialBall.getX(), initialBall.getY(),
                                                    new Vector2(initialBall.getDirection().x,
                                                                -initialBall.getDirection().y))
        );
    }
}
