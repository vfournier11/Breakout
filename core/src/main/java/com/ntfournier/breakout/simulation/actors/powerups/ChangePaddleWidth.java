package com.ntfournier.breakout.simulation.actors.powerups;

import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.simulation.Simulation;
import com.ntfournier.breakout.simulation.actors.*;

import java.util.ArrayList;

public class ChangePaddleWidth extends PowerUp {
    private float factor;

    public ChangePaddleWidth(float x, float y, float factor) {
        super(x, y);
        this.factor = factor;
    }

    @Override
    public void activate(Simulation simulation, Paddle paddle, ArrayList<Ball> balls) {
        float oldCenter = paddle.getX(Align.center);
        paddle.setWidth(paddle.getWidth() * factor);
        paddle.setX(oldCenter, Align.center);
    }
}
