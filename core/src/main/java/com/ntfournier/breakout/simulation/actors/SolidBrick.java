package com.ntfournier.breakout.simulation.actors;

import com.badlogic.gdx.graphics.Color;

public class SolidBrick extends Brick {
    private Color[] colors;

    public SolidBrick(float x, float y, int hitPoints, Color... colors) {
        super(x, y);
        this.setHitPoints(hitPoints);

        this.colors = colors;
    }

    @Override
    public Color getColor() {
        return this.colors[getHitPoints() - 1];
    }
}
