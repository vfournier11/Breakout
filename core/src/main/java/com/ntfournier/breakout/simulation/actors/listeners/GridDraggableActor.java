package com.ntfournier.breakout.simulation.actors.listeners;

import com.badlogic.gdx.scenes.scene2d.*;

public class GridDraggableActor extends DraggableActor {
    private int gridSize;

    public GridDraggableActor(Actor actor, int gridSize) {
        super(actor);
        this.gridSize = gridSize;
    }

    @Override
    public void touchDragged(InputEvent event, float x, float y, int pointer) {
        actor.setX((int) (actor.getX() + x + gridSize / 2) / gridSize * gridSize);
        actor.setY((int) (actor.getY() + y + gridSize / 2) / gridSize * gridSize);
    }
}
