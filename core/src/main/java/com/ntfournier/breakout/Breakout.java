package com.ntfournier.breakout;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.ntfournier.breakout.screens.MainMenuScreen;
import com.ntfournier.breakout.simulation.*;
import com.ntfournier.breakout.simulation.actors.*;
import com.ntfournier.breakout.simulation.actors.powerups.PowerUp;
import com.ntfournier.breakout.simulation.rulesset.RulesSet;
import com.ntfournier.breakout.utils.GraphicEffect;

import static com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP_PINGPONG;
import static com.ntfournier.breakout.Constants.*;

/**
 * Breakout, manage the screens and simulation.
 */
public class Breakout extends Game {

    public SpriteBatch batch;
    public Simulation simulation;

    public RulesSet rulesSet;

    public float deltaModifier = 1.0f;

    public TextureAtlas textureAtlas;

    public BitmapFont font;
    public NinePatch brickNineTexture;
    public NinePatch paddleNineTexture;
    public TextureRegion ballTexture;
    public Animation<TextureRegion> powerUpAnimation;

    public Paddle paddleControlHitZone;
    public GraphicEffect graphicEffect;

    public EventHandler eventHandler = new EventHandler();
    public float timeSkipped = 0;
    private boolean processUserInput = true;

    public void setProcessUserInput(boolean processUserInput) {
        this.processUserInput = processUserInput;
    }

    @Override
    public void create() {
        this.instantiateEventHandlers();

        this.textureAtlas = new TextureAtlas("breakout.txt");

        this.brickNineTexture = new NinePatch(this.textureAtlas.findRegion("brick"), 8, 8, 8, 8);
        this.paddleNineTexture = new NinePatch(this.textureAtlas.findRegion("paddle"), 8, 8, 8, 8);
        this.ballTexture = this.textureAtlas.findRegion("ball");
        this.powerUpAnimation = new Animation<>(0.16f, this.textureAtlas.findRegions("power_up"), LOOP_PINGPONG);

        this.batch = new SpriteBatch();

        this.deltaModifier = 1.0f;

        this.simulation = new Simulation(this.eventHandler,
                                         Constants.SIMULATION_WIDTH,
                                         Constants.SIMULATION_HEIGHT,
                                         new Stage(new FitViewport(Constants.SIMULATION_WIDTH,
                                                                   Constants.SIMULATION_HEIGHT),
                                                   this.batch));

        this.instantiatePaddleControls(); // TODO Remove ?

        font = new BitmapFont();
        font.getData().setScale(2);
        font.setColor(Color.PURPLE);

        this.graphicEffect = new GraphicEffect();

        this.setInitialScreen();
    }

    protected void setInitialScreen() {
        this.setScreen(new MainMenuScreen(this));
    }

    protected void instantiateEventHandlers() {
        this.eventHandler.put(Constants.EVENT_BALL_HIT_BOTTOM, (ball) -> this.rulesSet.ballHitBottom((Ball) ball[0]));
        this.eventHandler.put(Constants.EVENT_NO_BALL_LEFT, (obj) -> this.rulesSet.noBallsLeftInPlay());
        this.eventHandler.put(Constants.EVENT_BRICK_DESTROYED,
                              (brick) -> this.rulesSet.brickDestroyed((Brick) brick[0]));
        this.eventHandler.put(Constants.EVENT_PADDLE_HIT, (ball) -> this.rulesSet.ballHitPaddle((Ball) ball[0]));
        this.eventHandler.put(Constants.EVENT_NO_BRICK_LEFT, (obj) -> this.rulesSet.noBricksLeftInPlay());
        this.eventHandler.put(Constants.EVENT_BALL_CREATED, (ball) -> this.rulesSet.ballCreated((Ball) ball[0]));
        this.eventHandler.put(Constants.EVENT_POWER_UP_COLLECTED,
                              (powerUp) -> this.rulesSet.powerUpCollected((PowerUp) powerUp[0]));
    }

    public void renderSimulation() {
        this.simulation.stage.draw();

        this.batch.begin();
        for (int i = 0; i < this.simulation.getPowerUps().size(); ++i) {
            PowerUp powerUp = this.simulation.getPowerUps().get(i);
            this.drawActor(this.batch, powerUp, powerUpAnimation.getKeyFrame(powerUp.getStateTime()));
        }

        for (int i = 0; i < this.simulation.getBricks().size(); ++i) {
            this.drawActor(this.batch, this.simulation.getBricks().get(i), brickNineTexture);
        }

        this.drawActor(this.batch, this.simulation.getPaddle(), paddleNineTexture);
        for (int i = 0; i < this.simulation.getBalls().size(); ++i) {
            Ball ball = this.simulation.getBalls().get(i);
            this.drawActor(this.batch, ball, ballTexture);
            if (ball.getDirection().y < 0 && ball.getY() > Constants.PADDLE_Y) {
                this.graphicEffect.drawBallPrediction(this.simulation, ball, this.batch, ballTexture);
            }
        }

        font.draw(this.batch, this.rulesSet.getScoreString(), SIMULATION_WIDTH / 2 - 300, SIMULATION_HEIGHT - 300);
        this.batch.end();
    }

    public void handleInputs() {
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) && this.processUserInput) {
            this.simulation.getPaddle().setX(this.simulation.getPaddle().getX() + 200 * Gdx.graphics.getDeltaTime());
            this.paddleControlHitZone.setX(this.simulation.getPaddle().getX() + 200 * Gdx.graphics.getDeltaTime());
        }

        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) && this.processUserInput) {
            this.simulation.getPaddle().setX(this.simulation.getPaddle().getX() - 200 * Gdx.graphics.getDeltaTime());
            this.paddleControlHitZone.setX(this.simulation.getPaddle().getX() - 200 * Gdx.graphics.getDeltaTime());
        }
    }

    public void updateSimulation(float delta) {
        this.simulation.update(this.deltaModifier * delta);
    }

    private void drawActor(SpriteBatch batch, GameActor gameActor, NinePatch texture) {
        Color color = batch.getColor();
        batch.setColor(gameActor.getColor());
        texture.draw(batch, gameActor.getX(), gameActor.getY(), gameActor.getWidth(), gameActor.getHeight());
        batch.setColor(color);
    }

    private void drawActor(SpriteBatch batch, GameActor gameActor, TextureRegion texture) {
        Color color = batch.getColor();
        batch.setColor(gameActor.getColor());
        batch.draw(texture, gameActor.getX(), gameActor.getY(), gameActor.getWidth(), gameActor.getHeight());
        batch.setColor(color);
    }

    @Override
    public void dispose() {
        this.simulation.dispose();
        this.textureAtlas.dispose();
    }

    public void instantiatePaddleControls() {
        this.paddleControlHitZone = new Paddle(this.simulation.getPaddle().getX());

        this.paddleControlHitZone.setOuterBounds(new Rectangle(0,
                                                               0,
                                                               Constants.SIMULATION_WIDTH,
                                                               Constants.SIMULATION_HEIGHT));
        this.paddleControlHitZone.setY(this.simulation.getPaddle().getY() - 80);
        this.paddleControlHitZone.setHeight(Constants.PADDLE_HEIGHT + 160);
        this.simulation.stage.addActor(this.paddleControlHitZone);

        paddleControlHitZone.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (processUserInput) {
                    paddleControlHitZone.moveBy(x - PADDLE_WIDTH / 2, 0);
                    simulation.getPaddle().moveBy(x - PADDLE_WIDTH / 2, 0);
                }
                return true;
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                if (processUserInput) {
                    paddleControlHitZone.moveBy(x - PADDLE_WIDTH / 2, 0);
                    simulation.getPaddle().moveBy(x - PADDLE_WIDTH / 2, 0);
                }
            }
        });
    }

    public void setRulesSet(RulesSet rules) {
        this.rulesSet = rules;
    }

    public void startGame() {
        this.rulesSet.startGame();
    }

    public boolean isGameOver() {
        return this.rulesSet.isGameOver();
    }

    public void reset() {
        this.simulation.reset();
    }
}
