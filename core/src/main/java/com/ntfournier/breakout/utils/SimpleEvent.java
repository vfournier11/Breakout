package com.ntfournier.breakout.utils;

public interface SimpleEvent {
    public void handle(Object... objects);
}
