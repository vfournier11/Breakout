package com.ntfournier.breakout.utils;

import com.badlogic.gdx.graphics.Color;

public class RGBColor extends Color {
    public RGBColor(int r, int g, int b) {
        super(r / 255.0f, g / 255.0f, b / 255.0f, 1);
    }
}
