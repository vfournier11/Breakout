package com.ntfournier.breakout.utils;

import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.simulation.Simulation;
import com.ntfournier.breakout.simulation.actors.*;

import static com.ntfournier.breakout.Constants.BALL_SIZE;

public class GraphicEffect {
    public void drawBallPrediction(Simulation simulation, Ball ball, SpriteBatch batch, TextureRegion ballTexture) {
        float xPrediction = Utils.predictXCollision(ball, simulation.getPaddle());
        if (xPrediction > simulation.getPaddle().getX() - BALL_SIZE / 2 && xPrediction < simulation.getPaddle().getX(
                Align.right) + BALL_SIZE / 2) {
            int spacing = (int) (ball.getHeight() / 4 * 3);
            float opacity = .02f;
            int repetition = 16;
            for (int i = repetition; i > 0; --i) {
                opacity += .01f;
                batch.setColor(0, 0, opacity, opacity);
                batch.draw(ballTexture,
                           xPrediction - ball.getDirection().x * i * spacing,
                           simulation.getPaddle().getY(Align.top) - ball.getDirection().y * i * spacing,
                           ball.getWidth(),
                           ball.getHeight());
            }

            Vector2 bounceDirection = simulation.getPaddle().getBounceDirection(xPrediction + BALL_SIZE / 2);
            for (int i = 0; i < repetition; ++i) {
                opacity += 0.01f;
                batch.setColor(opacity, opacity, 0, opacity);
                batch.draw(ballTexture,
                           xPrediction + bounceDirection.x * i * spacing,
                           simulation.getPaddle().getY(Align.top) + bounceDirection.y * i * spacing,
                           ball.getWidth(),
                           ball.getHeight());
            }

            batch.setColor(1, 1, 1, 1);
        }
    }
}
