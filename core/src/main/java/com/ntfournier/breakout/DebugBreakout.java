package com.ntfournier.breakout;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.ntfournier.breakout.screens.*;
import com.ntfournier.breakout.simulation.actors.*;
import com.ntfournier.breakout.simulation.actors.listeners.GridDraggableActor;
import com.ntfournier.breakout.simulation.rulesset.SimpleRulesSet;

import static com.ntfournier.breakout.Constants.*;

public class DebugBreakout extends Breakout {
    private static final String DEBUG_STRING_FORMAT = "Debug Speed: %d \n And Patate";
    private final ScreenType initialScreen;

    private FPSLogger fpsLogger = new FPSLogger();
    private boolean isTimeStopped = false;
    private boolean nextFrame = false;

    private int timeModifier = 100;

    private BitmapFont debugFont;
    private String debugString = String.format(DEBUG_STRING_FORMAT, 100);

    public DebugBreakout(ScreenType initialScreen) {
        this.initialScreen = initialScreen;
    }

    @Override
    protected void setInitialScreen() {
        switch (this.initialScreen) {
            case MAIN_MENU:
                this.setScreen(new MainMenuScreen(this));
                break;
            case LEVEL_EDITOR:
                this.setScreen(new LevelEditorScreen(this));
                break;
            case GAME_SCREEN:
            default:
                this.setScreen(new GameScreen(this, new SimpleRulesSet(this.simulation, 1)));
                break;
        }
    }

    @Override
    public void create() {
        super.create();

        this.eventHandler.put(Constants.EVENT_BALL_CREATED, (objects) -> {
            simulation.stage.addActor((Ball) objects[0]);
        });

        debugFont = new BitmapFont();
        debugFont.setColor(Color.BLUE);

        simulation.stage.setDebugAll(true);
    }

    @Override
    protected void instantiateEventHandlers() {
        super.instantiateEventHandlers();

        this.eventHandler.put(Constants.EVENT_BRICK_CREATED, (bricks) -> {
            Brick brick = (Brick) (bricks[0]);
            brick.addListener(new GridDraggableActor(brick, BRICK_SIZE));
        });
    }

    @Override
    public void handleInputs() {
        super.handleInputs();

        if (Gdx.input.isKeyJustPressed(Input.Keys.B)) {
            int randomIndex = (int) (Math.random() * this.simulation.getBricks().size());
            this.simulation.removeBrick(this.simulation.getBricks().get(randomIndex));
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.P)) {
            this.isTimeStopped = !this.isTimeStopped;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.N)) {
            this.isTimeStopped = true;
            this.nextFrame = true;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
            this.simulation.actorsFactory.createBallOnPaddle();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.EQUALS)) {
            this.timeModifier += 10;
            this.debugString = String.format(DEBUG_STRING_FORMAT, this.timeModifier);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.MINUS) && this.timeModifier > 0.15f) {
            this.timeModifier -= 10;
            this.debugString = String.format(DEBUG_STRING_FORMAT, this.timeModifier);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_0)) {
            this.timeModifier = 100;
            this.debugString = String.format(DEBUG_STRING_FORMAT, this.timeModifier);
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.L)) {
            System.out.println("--------------------------------------------------------------------------------");
            System.out.println(this.simulation.getPaddle().getLogString());
            for (int i = 0; i < this.simulation.getBricks().size(); ++i) {
                System.out.println(this.simulation.getBricks().get(i).getLogString());
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.G)) {
            this.simulation.getPaddle().setWidth(Constants.SIMULATION_WIDTH);
        }
    }

    @Override
    public void renderSimulation() {
        super.renderSimulation();

        this.fpsLogger.log();

        this.batch.begin();
        this.debugFont.draw(this.batch,
                            this.debugString,
                            SIMULATION_WIDTH / 2 - 300,
                            SIMULATION_HEIGHT - SIMULATION_HEIGHT * .05f);
        this.batch.end();
    }

    @Override
    public void updateSimulation(float delta) {
        delta = (this.nextFrame) ? 1.0f / 60 : delta * timeModifier / 100.f * (this.isTimeStopped ? 0 : 1);
        super.updateSimulation(delta);
        this.nextFrame = false;
    }

    @Override
    public void instantiatePaddleControls() {
        super.instantiatePaddleControls();
    }
}
