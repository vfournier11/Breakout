package com.ntfournier.breakout.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.ntfournier.breakout.Breakout;
import com.ntfournier.breakout.simulation.rulesset.RulesSet;

public class GameScreen extends UIScreen {
    private static final String PLAY_MENU = "PLAY_MENU";
    private static final String PAUSE_MENU = "PAUSE_MENU";
    private static final String GAME_OVER_MENU = "GAME_OVER_MENU";

    public GameScreen(Breakout breakout, RulesSet rules) {
        super(breakout);
        this.breakout.setRulesSet(rules);
        this.breakout.startGame();
    }

    @Override
    protected void createUI() {
        createPlayMenu();
        createPauseMenu();
        createGameOverMenu();
    }

    @Override
    protected void setInitialMenu() {
        this.setActiveMenu(PLAY_MENU);
    }

    private void createPauseMenu() {
        Table pauseMenu = this.createMenu(PAUSE_MENU);

        HorizontalGroup horizontalGroup = new HorizontalGroup();
        pauseMenu.add(horizontalGroup);

        this.createTextButton(horizontalGroup, "Continue", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                setActiveMenu(PLAY_MENU);
            }
        });
    }

    @Override
    protected void setActiveMenu(String visibleMenu) {
        super.setActiveMenu(visibleMenu);
        breakout.setProcessUserInput(visibleMenu.equals(PLAY_MENU));
    }

    private void createGameOverMenu() {
        // Game Over label
        Table gameOverMenu = this.createMenu(GAME_OVER_MENU);

        HorizontalGroup horizontalGroup = new HorizontalGroup();
        gameOverMenu.addActor(horizontalGroup);

        horizontalGroup.addActor(new Label("GAME OVER", this.skin));

    }

    private void createPlayMenu() {
        Table playMenu = this.createMenu(PLAY_MENU);

        HorizontalGroup horizontalGroup = new HorizontalGroup();
        playMenu.addActor(horizontalGroup);

        this.createTextButton(horizontalGroup, "Pause", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                setActiveMenu(PAUSE_MENU);
            }
        });
    }

    @Override
    public void draw() {
        breakout.renderSimulation();
    }

    @Override
    public void update(float delta) {
//        delta = skipFrame(delta);

        breakout.handleInputs();

        if (activeMenu.equals(PLAY_MENU)) {
            breakout.updateSimulation(delta);

            if (breakout.isGameOver()) {
                setActiveMenu(GAME_OVER_MENU);
                breakout.setProcessUserInput(false);
            }
        }
    }

    private float skipFrame(float delta) {
        if (breakout.rulesSet.skipNextFrame) {
            breakout.timeSkipped += delta;
            if (breakout.timeSkipped >= (1.0f / 24)) {
                breakout.rulesSet.skipNextFrame = false;
            }

            delta = 0;
        } else if (breakout.timeSkipped > 0.0f) {
            delta = breakout.timeSkipped;
            breakout.timeSkipped = 0;
        }
        return delta;
    }
}
