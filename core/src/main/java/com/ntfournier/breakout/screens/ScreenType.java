package com.ntfournier.breakout.screens;

public enum ScreenType {
    MAIN_MENU,
    LEVEL_EDITOR,
    GAME_SCREEN
}
