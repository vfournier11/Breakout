package com.ntfournier.breakout.screens;

import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.ntfournier.breakout.*;

import java.util.*;

import static com.ntfournier.breakout.Constants.SKIN_LOCATION;

public abstract class UIScreen implements Screen {
    protected final Breakout breakout;
    // UI Elements
    protected Stage uiStage;
    protected Skin skin; // should be instantiated once

    protected Map<String, Table> menus = new HashMap<>();
    protected String activeMenu;

    private InputMultiplexer inputMultiplexer;

    public UIScreen(Breakout breakout) {
        this.breakout = breakout;

        this.uiStage = new Stage(new FitViewport(Constants.SIMULATION_WIDTH,
                                                 Constants.SIMULATION_HEIGHT));

        this.inputMultiplexer = new InputMultiplexer(this.uiStage, this.breakout.simulation.stage);
        Gdx.input.setInputProcessor(this.inputMultiplexer);

        this.skin = new Skin(Gdx.files.internal(SKIN_LOCATION));
        this.createUI();
        this.setInitialMenu();
    }

    protected void setActiveMenu(String visibleMenu) {
        this.menus.forEach((name, menu) -> menu.setVisible(false));
        this.menus.get(visibleMenu).setVisible(true);
        this.activeMenu = visibleMenu;
    }

    protected TextButton createTextButton(Group parent, String text, ChangeListener onClick) {
        TextButton textButton = new TextButton(text, this.skin);
        textButton.addListener(onClick);
        parent.addActor(textButton);
        return textButton;
    }

    protected Table createMenu(String name) {
        Table menu = new Table();
        menu.setFillParent(true);
        menu.top();
        this.uiStage.addActor(menu);
        this.menus.put(name, menu);
        return menu;
    }

    protected abstract void createUI();

    protected abstract void setInitialMenu();

    protected abstract void update(float delta);

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        this.uiStage.act();
        this.uiStage.draw();

        this.draw();

        this.update(Gdx.graphics.getDeltaTime());
    }

    protected abstract void draw();

    @Override
    public void resize(int width, int height) {
        this.uiStage.getViewport().update(width, height);
        this.breakout.simulation.stage.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        this.inputMultiplexer.clear();
        this.uiStage.dispose();
    }
}
