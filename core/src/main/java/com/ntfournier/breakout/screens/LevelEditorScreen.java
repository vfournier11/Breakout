package com.ntfournier.breakout.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.ntfournier.breakout.Breakout;
import com.ntfournier.breakout.simulation.rulesset.LevelEditorRulesSet;

public class LevelEditorScreen extends UIScreen {
    private static final String EDITOR_MENU = "EDITOR_MENU";
    private static final String PLAY_TEST_MENU = "PLAY_TEST_MENU";

    public LevelEditorScreen(Breakout breakout) {
        super(breakout);
        this.breakout.setRulesSet(new LevelEditorRulesSet(breakout.simulation, 0));
    }

    @Override
    protected void createUI() {
        this.createEditorMenu();
        this.createPlayTestMenu();
    }

    @Override
    protected void setInitialMenu() {
        this.setActiveMenu(EDITOR_MENU);
    }

    private void createEditorMenu() {
        Table editorMenu = this.createMenu(EDITOR_MENU);

        HorizontalGroup bottomBar = new HorizontalGroup();
        editorMenu.addActor(bottomBar);
        bottomBar.bottom();

        this.createTextButton(bottomBar, "X", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                breakout.setScreen(new MainMenuScreen(breakout));
            }
        });

        this.uiStage.addActor(editorMenu);
    }

    private void createPlayTestMenu() {
        Table playTestMenu = new Table(this.skin);
        playTestMenu.setFillParent(true);
        playTestMenu.top();
        this.menus.put(PLAY_TEST_MENU, playTestMenu);

        this.uiStage.addActor(playTestMenu);

        this.createTextButton(playTestMenu, "Reset", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                breakout.reset();
            }
        });
        this.createTextButton(playTestMenu, "Edit", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                setActiveMenu(EDITOR_MENU);
            }
        });
    }

    @Override
    protected void update(float delta) {

    }

    @Override
    protected void draw() {
        breakout.renderSimulation();

    }
}
