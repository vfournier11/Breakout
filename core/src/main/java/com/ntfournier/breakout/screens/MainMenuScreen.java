package com.ntfournier.breakout.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.ntfournier.breakout.Breakout;
import com.ntfournier.breakout.simulation.rulesset.SimpleRulesSet;

public class MainMenuScreen extends UIScreen {
    public MainMenuScreen(Breakout breakout) {
        super(breakout);
    }

    @Override
    protected void draw() {

    }

    @Override
    protected void createUI() {
        Table table = new Table();
        table.setFillParent(true);
        uiStage.addActor(table);

        HorizontalGroup horizontalGroup = new HorizontalGroup();

        TextButton buttonPlay = new TextButton("Play", this.skin);
        buttonPlay.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                breakout.setScreen(new GameScreen(breakout, new SimpleRulesSet(breakout.simulation, 1)));
                dispose();
            }
        });
        horizontalGroup.addActor(buttonPlay);

        table.add(horizontalGroup);
    }

    @Override
    protected void setInitialMenu() {

    }

    @Override
    protected void update(float delta) {

    }
}
