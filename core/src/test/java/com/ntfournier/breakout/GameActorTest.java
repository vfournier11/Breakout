package com.ntfournier.breakout;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.simulation.actors.*;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;


class GameActorTest {

    @Test
    void log() {

//        GameActor gameActor = new GameActor(new Rectangle(0, 0, 50, 100), 100, new Vector2(1, 1));
//        gameActor.direction.x = 1.00f;
//        gameActor.direction.y = 1.5f;
//        assertEquals("GameActor           x:     0.00  y:     0.00  w:    50.00  h:   100.00  dir:  1.00, 1.50", gameActor.getLogString());
    }

    @Test
    void getNineAreaCollision() {
        Brick brick = new Brick(100, 100);
        brick.setWidth(100);
        brick.setHeight(100);

        Ball ball = new Ball(0, 0, new Vector2(0, 0));
        ball.setWidth(20);
        ball.setHeight(20);

        assertEquals(0, brick.getNineAreaCollision(ball));

        ball.setX(101, Align.right);
        ball.setY(101, Align.top);

        assertEquals(Align.bottomLeft, brick.getNineAreaCollision(ball));

        ball.setX(101, Align.right);
        ball.setY(111, Align.top);

        assertEquals(Align.left, brick.getNineAreaCollision(ball));

        ball.setX(199);
        ball.setY(111, Align.top);

        assertEquals(Align.right, brick.getNineAreaCollision(ball));
    }
}