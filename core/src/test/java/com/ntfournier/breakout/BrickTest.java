package com.ntfournier.breakout;

import com.badlogic.gdx.utils.Align;
import com.ntfournier.breakout.simulation.actors.GameActor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BrickTest {

    @Test
    void getCollision() {
        GameActor brick = new GameActor(100, 100, 20, 20);
        GameActor ball = new GameActor(92, 92, 10, 10);
        assertEquals(Align.topLeft, brick.getNineAreaCollision(ball));
    }
}