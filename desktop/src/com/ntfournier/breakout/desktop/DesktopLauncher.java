package com.ntfournier.breakout.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.ntfournier.breakout.*;
import com.ntfournier.breakout.screens.*;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.height = 1280;
        config.width = 720;
        config.vSyncEnabled = false;
        config.foregroundFPS = 180;

        new LwjglApplication(new DebugBreakout(ScreenType.LEVEL_EDITOR), config);
    }
}
